ARG NEXTCLOUD_VERSION
FROM docker.io/library/nextcloud:${NEXTCLOUD_VERSION}-apache

RUN apt update && apt install -yqq procps smbclient supervisor \
&& rm -rf /var/lib/apt/lists/* \
&& mkdir /var/log/supervisord /var/run/supervisord

COPY supervisord.conf /

ENV NEXTCLOUD_UPDATE=1

CMD ["/usr/bin/supervisord", "-c", "/supervisord.conf"]
