set -e
IMAGE_NAME="nextcloud"
MANIFEST_NAME="$IMAGE_NAME:${NEXTCLOUD_VERSION}"
REGISTRY="registry.gitlab.com/home-webserver"

buildah manifest create nextcloud:${NEXTCLOUD_VERSION}

buildah build --platform linux/arm64,linux/amd64,linux/arm --manifest nextcloud:${NEXTCLOUD_VERSION} --build-arg NEXTCLOUD_VERSION=$NEXTCLOUD_VERSION -f Dockerfile .

buildah manifest push --all nextcloud:${NEXTCLOUD_VERSION} docker://$REGISTRY/${IMAGE_NAME}:${NEXTCLOUD_VERSION}
